package com.example.springbootbackend.Controller;


import com.example.springbootbackend.controller.EmployeeController;
import com.example.springbootbackend.dto.EmployeeDTO;
import com.example.springbootbackend.entities.mysql.Employee;
import com.example.springbootbackend.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
//import com.unittesting.UnitTesting.model.Response;
import org.json.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
//import static org.springframework.test.web.servlet.ResultActionsl=

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(controllers = EmployeeController.class)
@AutoConfigureMockMvc
//@ActiveProfiles("test")
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private EmployeeService employeeService;

    @InjectMocks
    private EmployeeController employeeController;

    private List<Employee> employeesTest;

    private List<EmployeeDTO> employeesDTO;

    ObjectMapper obMap = new ObjectMapper();

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();

        this.employeesDTO = new ArrayList<>();
        this.employeesDTO.add(new EmployeeDTO(1, "Jatin", "Arora", "jatinarora@gmail.com"));
        this.employeesDTO.add(new EmployeeDTO(2, "Nishta", "Arora", "nishtaarora@gmail.com"));
        this.employeesDTO.add(new EmployeeDTO(3, "Animesh", "Madaan", "animeshmadaan@gmail.com"));

        /*
        this.employeesTest = new ArrayList<>();
        this.employeesTest.add(new Employee(1, "Jatin", "Arora", "jatinarora@gmail.com"));
        this.employeesTest.add(new Employee(1, "Nishta", "Arora", "nishtaarora@gmail.com"));
        this.employeesTest.add(new Employee(1, "Animesh", "Madaan", "animeshmadaan@gmail.com"));

         */
    }

    @Test
    public void saveEmployee() throws Exception{
        EmployeeDTO employeeDTO = new EmployeeDTO(4, "Nimesh", "Babbar", "nimeshbabbar@gmail.com");
        //String jsonRequest = obMap.writeValueAsString(EmployeeDTO);

        MvcResult result = mockMvc.perform(post("/api/employee")
                .content(String.valueOf(employeeDTO))
                .contentType(MediaType.APPLICATION_JSON)).
                andReturn();

        String res = result.getResponse().getContentAsString();
        //Response response = obMap.readValue(res, Response.class);
        //Assert.assertTrue(response.isSuccess() == Boolean.TRUE);
    }


    @Test
    public void deleteEmployee() throws Exception{
        long id = 2;
        String url = "/api/employees/{id}";
        //doNothing().when(employeeService.deleteEmployee(id));
        //when(employeeService.deleteEmployee(id)).then(employeesDTO.remove((int)id));
        //System.out.println("tajryyjtduyt" + employeeService.getAllEmployee().size());

        mockMvc.perform(MockMvcRequestBuilders
                .delete(url, id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


        //System.out.println(employeeService.getAllEmployee().size());

        //verify(employeeService, times(1)).deleteEmployee(id);

    }

    @Test
    public void getEmployeeById() throws Exception{
        long id = 2;
        when(employeeService.getEmployeeById(id)).thenReturn(employeesDTO.get((int)id));
        System.out.println(employeeService.getAllEmployee());
        String url = "/api/employees/{id}";
        mockMvc.perform(get(url, id)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(employeeService.getEmployeeById(id).getFirstName()));

    }


    @Test
    public void getAllEmployeeTest() throws Exception{
        //System.out.println("Test1");
        //given(employeeService.getAllEmployee()).willReturn(employeesDTO);
        //System.out.println(doReturn (employeesTest).when(employeeService.getAllEmployee()));
        when(employeeService.getAllEmployee()).thenReturn(employeesDTO);
        //System.out.println(employeeService.getAllEmployee().getClass().getName());
        //System.out.println(employeeService.getAllEmployee());
        /*
        this.mockMvc.perform(
                get("/api/employees")
        )
                .andExpect(status().isOk());
                //.andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(employeesDTO));



         */
        String url = "/api/employees";
        /*
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(url);
        System.out.println(requestBuilder);



        MvcResult mvcResult = mockMvc
                .perform(MockMvcRequestBuilders.get(url))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        System.out.println(mvcResult.getResponse().getContentAsString().getClass().getName());
        System.out.println(mvcResult.getResponse());
        JSONArray.length(mvcResult.getResponse());

*/


        //Assert.assertEquals(mvcResult.getResponse().getContentAsString(), employeeService.getAllEmployee().size());

        mockMvc.perform(get(url)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(employeeService.getAllEmployee().size()));



    }





}
