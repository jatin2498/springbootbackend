package com.example.springbootbackend.Service;


import com.example.springbootbackend.dto.EmployeeDTO;
import com.example.springbootbackend.entities.mysql.Employee;
import com.example.springbootbackend.repository.EmployeeRepository;
import com.example.springbootbackend.service.EmployeeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceTest {

    @Autowired
    private EmployeeService employeeService;

    @MockBean
    private EmployeeRepository employeeRepository;


/*
    @Before
    public void setUp() throws Exception {
        //mockMvc = MockMvcBuilders.webAppContextSetup(context).build();

        this.employeesDTO = new ArrayList<>();
        this.employeesDTO.add(new EmployeeDTO(1, "Jatin", "Arora", "jatinarora@gmail.com"));
        this.employeesDTO.add(new EmployeeDTO(2, "Nishta", "Arora", "nishtaarora@gmail.com"));
        this.employeesDTO.add(new EmployeeDTO(3, "Animesh", "Madaan", "animeshmadaan@gmail.com"));

        /*
        this.employeesTest = new ArrayList<>();
        this.employeesTest.add(new Employee(1, "Jatin", "Arora", "jatinarora@gmail.com"));
        this.employeesTest.add(new Employee(1, "Nishta", "Arora", "nishtaarora@gmail.com"));
        this.employeesTest.add(new Employee(1, "Animesh", "Madaan", "animeshmadaan@gmail.com"));


    }
*/



    @Test
    public void getAllEmployeesTest(){
        List<Employee> employeesDTO = new ArrayList<>();
        employeesDTO.add(new Employee(1, "Jatin", "Arora", "jatinarora@gmail.com"));
        employeesDTO.add(new Employee(2, "Nishta", "Arora", "nishtaarora@gmail.com"));
        employeesDTO.add(new Employee(3, "Animesh", "Madaan", "animeshmadaan@gmail.com"));
        //System.out.println(employeesDTO);
        when(employeeRepository.findAll()).thenReturn(employeesDTO);
        //when(employeeRepository.findAll()).thenReturn(employeesDTO);
        assertEquals(3, employeeService.getAllEmployee().size());
    }




    @Test
    public void getEmployeeByIdTest(){
        //EmployeeDTO employeeDTO = new EmployeeDTO(1L, "jatin", "arora", "jatinarora@gmail.com");
        Employee employee = new Employee(1L, "jatin", "arora", "jatinarora@gmail.com");
        when(employeeRepository.findById(1L)).thenReturn(java.util.Optional.of(employee));

        assertEquals(1L, employeeService.getEmployeeById(1L).getId());
    }

    @Test
    public void saveEmployeeTest(){
        Employee employee = new Employee(1L, "jatin", "arora", "jatinarora@gmail.com");
        when(employeeRepository.save(employee)).thenReturn(employee);
        assertEquals(employee.getFirstName(),employeeService.saveEmployee(employee).getFirstName());
    }


    @Test
    public void updateEmployeeTest(){
        Employee employee = new Employee(1L, "jatin", "arora", "jatinarora@gmail.com");
        when(employeeRepository.findById(1L)).thenReturn(java.util.Optional.of(new Employee(1L, "updatedjatin", "arora", "jatinarora@gmail.com")));
        Employee updatedEmployee = new Employee(1L, "updatedjatin", "arora", "jatinarora@gmail.com");
        when(employeeRepository.save(updatedEmployee)).thenReturn(updatedEmployee);
        assertEquals(updatedEmployee.getFirstName(), employeeService.updateEmployee(1L, updatedEmployee).getFirstName());
    }


    public void deleteEmployee(){
        Employee employee = new Employee(1L, "jatin", "arora", "jatinarora@gmail.com");
        when(employeeRepository.findById(1L)).thenReturn(java.util.Optional.of(employee));
    }

}
