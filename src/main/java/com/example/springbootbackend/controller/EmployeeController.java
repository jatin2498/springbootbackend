package com.example.springbootbackend.controller;

import com.example.springbootbackend.dto.EmployeeDTO;
import com.example.springbootbackend.entities.mysql.Employee;
import com.example.springbootbackend.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {
    private EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        super();
        this.employeeService = employeeService;
    }

    @PostMapping
    public EmployeeDTO saveEmployee(@RequestBody Employee employee){

        return employeeService.saveEmployee(employee);
    }

    @GetMapping
    public List<EmployeeDTO> getAllEmployee(){
        return employeeService.getAllEmployee();
    }


    @GetMapping("{id}")
    public EmployeeDTO getEmployeeById(@PathVariable("id") long employeeId){
        //System.out.println("test3" + employeeService.getEmployeeById(employeeId));
        return employeeService.getEmployeeById(employeeId);
    }


    @PutMapping("{id}")
    public EmployeeDTO updateEmployeeById(@PathVariable("id") long employeeId, @RequestBody Employee employee){
        return employeeService.updateEmployee(employeeId, employee);
    }


    @DeleteMapping
    public ResponseEntity<String> deleteEmployeeById(@PathVariable("id") long employeeId, @RequestBody Employee employee){
        employeeService.deleteEmployee(employeeId);
        return new ResponseEntity<String>("Employee Deleted Successfully", HttpStatus.OK);
    }

}
