package com.example.springbootbackend.service;


import com.example.springbootbackend.dto.EmployeeDTO;
import com.example.springbootbackend.entities.mysql.Employee;

import java.util.List;

public interface EmployeeService {
    EmployeeDTO saveEmployee(Employee employee);

    List<EmployeeDTO> getAllEmployee();

    EmployeeDTO getEmployeeById(long id);

    EmployeeDTO updateEmployee(long employeeId, Employee employee);

    Void deleteEmployee(long employeeId);

    //EmployeeDTO convertEntityToDTO(Employee employee);


    /*
    private EmployeeDTO convertEntityToData(Employee employee){

        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setFirstName(employee.getFirstName());
        employeeDTO.setLastName(employee.getLastName());
        employeeDTO.setEmail(employee.getEmail());
        return employeeDTO;
    }

     */
}
